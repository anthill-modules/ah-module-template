# Anthill Module Template

## Cheat Sheet

- Create remote Git repo with gitignore and Readme file
- Clone repo to computer
- In the folder run ```npm init``` and fill out as necessary
- Install test module ```npm i tap --save-dev```
- Create test folder with a *_test.js file
- Add test commands to package.json (see below)
- Create the main file, e.g. index.js
- Write your tests and the code to make them pass
- Check coverage report to find missing tests
- Setup CI (GitLab or Jenkins)
- Publish to NPM: ```npm publish```

## Test commands
Make sure the package.json file has the following:
```
"scripts": {
    "cover": "./node_modules/tap/bin/run.js test/*_test.js --cov --coverage-report=lcov",
    "test": "./node_modules/tap/bin/run.js --coverage-report=text-summary test/*_test.js"
},
```

## Notes

Use a namespace in the name so we can easily find our own modules,
e.g. __anthill-__.

To publish to NPM you need to have an account and be logged in:
https://www.npmjs.com/ 